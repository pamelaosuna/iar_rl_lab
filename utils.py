import os
import torch 

def save_actor(actor,policy_type,env_name,team_name,score=0):
    directory = os.getcwd() + '/data/policies/'
    filename = directory + env_name + '#' + team_name + '_' + str(score) \
            + '#' + policy_type + '#' + str(None)+ '#' + str(score) + '.pt'
    traced = torch.jit.script(actor)
    torch.jit.save(traced, filename)

def save_critic(critic,policy_type,env_name,team_name,name,score=0):
    directory = os.getcwd() + '/data/critics/critic_'
    filename = directory + env_name + '#' + team_name + '_' + str(score) \
            + '#' + policy_type + '#' + str(None)+ '#' + str(score) + name + '.pt'
    torch.save(critic,filename)