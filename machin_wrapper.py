from generic_net import GenericNet
import torch 

class MachinWrapper(GenericNet):

    def __init__(self,model):
        super(MachinWrapper, self).__init__()
        self.model = model 

    def select_action(self,state,deterministic):
        state = torch.from_numpy(state)
        action = (self.model.act({"state": state})[0])
    
        return [action.item()]


        
