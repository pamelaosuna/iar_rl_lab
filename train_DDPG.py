from machin.frame.algorithms import DDPG
from machin.utils.logging import default_logger as logger
from torch.distributions import Categorical
from typing import List
from utils import save_actor,save_critic

import torch as t
import torch.nn as nn
import gym
import numpy as np 

# configurations
"""env = gym.make("CartPole-v0")
observe_dim = 4
action_num = 2
max_episodes = 1
max_steps = 200
solved_reward = 190
solved_repeat = 5"""

# execution identifiers
algo = 'ddpg'
particularity = ''

# configurations
env = gym.make("Pendulum-v0")
observe_dim = 3
action_dim = 1
action_range = 2
max_episodes = 500
max_steps = 200
noise_param = (0, 0.2)
noise_mode = "normal"
solved_reward = -150
solved_repeat = 5

best_reward = None
all_rewards = np.zeros(max_episodes+1)
actor_loss = []
critic_loss = []

# model definition
class Actor(nn.Module):
    def __init__(self, state_dim, action_dim, action_range):
        super(Actor, self).__init__()

        self.fc1 = nn.Linear(state_dim, 16)
        self.fc2 = nn.Linear(16, 16)
        self.fc3 = nn.Linear(16, action_dim)
        self.action_range = action_range

    def forward(self, state):
        state = state.float()
        a = t.relu(self.fc1(state))
        a = t.relu(self.fc2(a))
        a = t.tanh(self.fc3(a)) * self.action_range
        return a

    @t.jit.export
    def select_action(self, state: List[float], deterministic: bool=False) -> List[float]:
        state = t.tensor(state)
        action = self.forward(state)
        act: List[float] = action.data.tolist()
        return act

class Critic(nn.Module):
    def __init__(self, state_dim, action_dim):
        super(Critic, self).__init__()

        self.fc1 = nn.Linear(state_dim + action_dim, 16)
        self.fc2 = nn.Linear(16, 16)
        self.fc3 = nn.Linear(16, 1)

    def forward(self, state, action):
        state_action = t.cat([state, action], 1)
        q = t.relu(self.fc1(state_action))
        q = t.relu(self.fc2(q))
        q = self.fc3(q)
        return q


if __name__ == "__main__":
    actor = Actor(observe_dim, action_dim, action_range)
    actor_t = Actor(observe_dim, action_dim, action_range)
    critic = Critic(observe_dim, action_dim)
    critic_t = Critic(observe_dim, action_dim)

    ddpg = DDPG(actor, actor_t, critic, critic_t,
                t.optim.Adam,
                nn.MSELoss(reduction='sum'))

    episode, step, reward_fulfilled = 0, 0, 0
    smoothed_total_reward = 0
    save_actor(actor,'ddpg_start', 'Pendulum-v0', 'Osuna-Serris', 0)
    save_critic(critic,'ddpg_start', 'Pendulum-v0', 'Osuna-Serris','ante')

    while episode < max_episodes:
        episode += 1
        total_reward = 0
        terminal = False
        step = 0
        state = t.tensor(env.reset(), dtype=t.float32).view(1, observe_dim)

        while not terminal and step <= max_steps:
            step += 1
            with t.no_grad():
                old_state = state
                # agent model inference
                action = ddpg.act_with_noise(
                            {"state": old_state},
                            noise_param=noise_param,
                            mode=noise_mode
                        )
                state, reward, terminal, _ = env.step(action.numpy())
                #env.render()
                state = t.tensor(state, dtype=t.float32).view(1, observe_dim)
                total_reward += reward[0]  # if pendulum env
                #total_reward += reward      # if mountaincar env

                ddpg.store_transition({
                    "state": {"state": old_state},
                    "action": {"action": action},
                    "next_state": {"state": state},
                    "reward": reward[0],   # if pendulum env
                    #"reward": reward,       # if mountaincar env
                    "terminal": terminal or step == max_steps
                })
        all_rewards[episode] = total_reward
        # update, update more if episode is longer, else less
        if episode > 100:
            for _ in range(step):
                a_loss,c_loss = ddpg.update()
                actor_loss.append(a_loss)
                critic_loss.append(c_loss)
            if best_reward is None or total_reward > best_reward:
                save_actor(actor,'ddpg_end', 'Pendulum-v0', 'Osuna-Serris', 0)

        # show reward
        smoothed_total_reward = (smoothed_total_reward * 0.9 +
                                 total_reward * 0.1)
        logger.info("Episode {} total reward={:.2f}"
                    .format(episode, smoothed_total_reward))

        if episode == max_episodes:
            save_critic(critic,'ddpg_end', 'Pendulum-v0', 'Osuna-Serris','ante')
        # if smoothed_total_reward > solved_reward:
        #     reward_fulfilled += 1
        #     if reward_fulfilled >= solved_repeat:
        #         logger.info("Environment solved!")
        #         exit(0)
        # else:
        #     reward_fulfilled = 0
    actor_loss = np.array(actor_loss)
    critic_loss = np.array(critic_loss)
    with open('rewards_' + algo + '_' + str(max_episodes) + '_' + particularity + '_' + '.npy', 'wb') as f:
        np.save(f, all_rewards)
    with open('actor_loss_' + algo + '_' + str(max_episodes) + '_' + particularity + '_' + '.npy', 'wb') as f:
        np.save(f, actor_loss)
    with open('critic_loss_' + algo + '_' + str(max_episodes) + '_' + particularity + '_' + '.npy', 'wb') as f:
        np.save(f, critic_loss)