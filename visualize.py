
import numpy as np
import matplotlib.pyplot as plt
import os
import numpy as np
from itertools import count
from policy_wrapper import PolicyWrapper
from generic_net import GenericNet
from environment import make_env
from train_DDPG import Actor, Critic
from itertools import product
import torch
import seaborn as sns
sns.set()


def visu_functions():
    folder_path = 'data/save/'
    algo = 'sac'
    max_ep = '500'
    partic = ''

    actor_loss_path = folder_path + 'actor_loss_' + algo + '_' + max_ep + '_' + partic + '_'
    critic_loss_path = folder_path + 'critic_loss_' + algo + '_' + max_ep + '_' + partic + '_'
    rewards_path = folder_path + 'rewards_' + algo + '_' + max_ep + '_' + partic + '_'

    actor_loss = np.load(actor_loss_path + '.npy')
    critic_loss = np.load(critic_loss_path + '.npy')
    rewards = np.load(rewards_path + '.npy')


    arrays = [actor_loss,critic_loss,rewards]
    names = ["Actor loss evolution","Critic loss evolution","Reward evolution"]
    ylabels = ["Loss", "Loss", "Reward"]
    paths = [actor_loss_path, critic_loss_path, rewards_path]

    for i in range(len(arrays)) :
        plt.plot(arrays[i])
        plt.title(names[i])
        plt.xlabel("Episodes")
        plt.ylabel(ylabels[i])
        plt.savefig(paths[i]+ '.png')
        plt.show()

#visu_functions()



def visu_heatmaps(actor_file,critic_file,resolution,fixed_action=None):
    critic = torch.load(critic_file)
    actor = torch.load(actor_file)
    angle_range = np.linspace(-np.pi, np.pi, resolution)
    vel = np.linspace(-8,8,resolution)


    states = product(angle_range,vel)
    states = torch.tensor([[np.cos(angle),np.sin(angle),vel] for angle,vel, in states]).float()

    actions = torch.tensor([actor.select_action(s.data.tolist()) for s in states]).float()
    if fixed_action is not None:
        Q_sa = critic.forward(states,torch.ones(resolution*resolution).unsqueeze(1)*fixed_action)
    else : 
        Q_sa = critic.forward(states,actions)

    #test
    angle, vitesse = 0, 0
    state = [np.cos(angle),np.sin(angle), vitesse]
    print('0,0,', actor.select_action(state, deterministic = False))

    angle, vitesse = 0.5, 0
    state = [np.cos(angle),np.sin(angle), vitesse]
    print('0.5, 0', actor.select_action(state, deterministic = False))


    Q_image = Q_sa.reshape((resolution,resolution)).detach().numpy()
    action_image = actions.reshape((resolution,resolution)).detach().numpy()
    
    plt.title('Actor')
    plt.imshow(action_image, cmap = 'PuOr')
    #plt.colorbar()
    cbar = plt.colorbar()
    cbar.set_label('Action')
    plt.xlabel("Vitesse")
    ranges = np.round(np.linspace(-8, 8, 5), 2)
    plt.xticks(np.linspace(0,resolution, 5), ranges)
    plt.ylabel("Angle")
    ranges = np.round(np.linspace(-np.pi, np.pi, 5), 2)
    plt.yticks(np.linspace(0,resolution, 5), ranges)
    plt.gca().invert_yaxis()
    plt.show()

    plt.title('Critic')
    plt.imshow(Q_image, cmap = 'PuOr')
    #plt.colorbar()
    cbar = plt.colorbar()
    cbar.set_label('Q-valeur')
    plt.xlabel("Vitesse")
    ranges = np.round(np.linspace(-8, 8, 5), 2)
    plt.xticks(np.linspace(0,resolution, 5), ranges)
    plt.ylabel("Angle")
    ranges = np.round(np.linspace(-np.pi, np.pi, 5), 2)
    plt.yticks(np.linspace(0,resolution, 5), ranges)
    plt.gca().invert_yaxis()
    plt.show()



# before
# actor_file = 'data/policies/Pendulum-v0#Osuna-Serris_0#ddpg#None#0.pt'
# critic_file = 'data/critic/critic_Pendulum-v0#Osuna-Serris_0#ddpg#None#0ante.pt'

#critic_file = 'data/critics/critic_Pendulum-v0#Osuna-Serris_0#ddpg_start#None#0ante.pt'
#actor_file = 'data/policies/Pendulum-v0#Osuna-Serris_0#ddpg_start#None#0.pt'

# after + normal
actor_file = 'data/policies/Pendulum-v0#Osuna-Serris_0#ddpg_end#None#0.pt'
critic_file = 'data/critics/critic_Pendulum-v0#Osuna-Serris_0#ddpg_end#None#0ante.pt'



visu_heatmaps(actor_file,critic_file,100)
