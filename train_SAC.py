from machin.frame.algorithms import SAC
from machin.utils.logging import default_logger as logger
from torch.nn.functional import softplus
from torch.distributions import Normal
from torch.distributions import Categorical
from machin_wrapper import MachinWrapper
from policy_wrapper import PolicyWrapper
from typing import List
from utils import save_actor, save_critic


import torch as t
import torch.nn as nn
import gym
import numpy as np


# execution identifiers
algo = 'sac'
particularity = ''

# configurations
env = gym.make("Pendulum-v0")
observe_dim = 3
action_dim = 1
action_range = 2
max_episodes = 500
# max_episodes = 20
max_steps = 200
noise_param = (0, 0.2)
noise_mode = "normal"
solved_reward = -150
solved_repeat = 5

best_reward = None
all_rewards = np.zeros(max_episodes+1)
actor_loss = []
critic_loss = []

def atanh(x):
    return 0.5 * t.log((1 + x) / (1 - x))


# model definition
class Actor(nn.Module):
    def __init__(self, state_dim, action_dim, action_range):
        super(Actor, self).__init__()

        self.fc1 = nn.Linear(state_dim, 16)
        self.fc2 = nn.Linear(16, 16)
        self.mu_head = nn.Linear(16, action_dim)
        self.sigma_head = nn.Linear(16, action_dim)
        self.action_range = action_range

    @t.jit.ignore
    def forward(self, state, action=None):
        state = state.float()
        a = t.relu(self.fc1(state))
        a = t.relu(self.fc2(a))
        mu = self.mu_head(a)
        sigma = softplus(self.sigma_head(a))
        dist = Normal(mu, sigma)
        act = (atanh(action / self.action_range)
               if action is not None
               else dist.rsample())
        act_entropy = dist.entropy()

        # the suggested way to confine your actions within a valid range
        # is not clamping, but remapping the distribution
        act_log_prob = dist.log_prob(act)
        act_tanh = t.tanh(act)
        act = act_tanh * self.action_range

        # the distribution remapping process used in the original essay.
        act_log_prob -= t.log(self.action_range *
                              (1 - act_tanh.pow(2)) +
                              1e-6)
        act_log_prob = act_log_prob.sum(1, keepdim=True)

        # If your distribution is different from "Normal" then you may either:
        # 1. deduce the remapping function for your distribution and clamping
        #    function such as tanh
        # 2. clamp you action, but please take care:
        #    1. do not clamp actions before calculating their log probability,
        #       because the log probability of clamped actions might will be
        #       extremely small, and will cause nan
        #    2. do not clamp actions after sampling and before storing them in
        #       the replay buffer, because during update, log probability will
        #       be re-evaluated they might also be extremely small, and network
        #       will "nan". (might happen in PPO, not in SAC because there is
        #       no re-evaluation)
        # Only clamp actions sent to the environment, this is equivalent to
        # change the action reward distribution, will not cause "nan", but
        # this makes your training environment further differ from you real
        # environment.
        return act, act_log_prob, act_entropy
    

    def deterministic_forward(self, state):
        state = state.float()
        a = t.relu(self.fc1(state))
        a = t.relu(self.fc2(a))
        mu = self.mu_head(a)
        #print(mu)
        act = mu
        act_tanh = t.tanh(act)
        act = act_tanh * self.action_range
        return act

    def stochastic_forward(self, state):
        state = state.float()
        a = t.relu(self.fc1(state))
        a = t.relu(self.fc2(a))
        mu = self.mu_head(a)
        sigma = softplus(self.sigma_head(a))
        #dist = Normal(mu, sigma)
        dist = t.randn(list(mu.size())[0])
        dist = dist*sigma + mu
        # act = (atanh(action / self.action_range)
        #       if action is not None
        #       else dist)
        act = dist
        # the suggested way to confine your actions within a valid range
        # is not clamping, but remapping the distribution
        act_tanh = t.tanh(act)
        act = act_tanh * self.action_range
        return act

    @t.jit.export
    def select_action(self, state: List[float], deterministic: bool=False) -> List[float]:
        state = t.tensor(state)
        if deterministic:
            act = self.deterministic_forward(state)
        else:
            act = self.stochastic_forward(state)
        act: List[float] = act.data.tolist()
        return act


class Critic(nn.Module):
    def __init__(self, state_dim, action_dim):
        super(Critic, self).__init__()

        self.fc1 = nn.Linear(state_dim + action_dim, 16)
        self.fc2 = nn.Linear(16, 16)
        self.fc3 = nn.Linear(16, 1)

    def forward(self, state, action):
        state_action = t.cat([state, action], 1)
        q = t.relu(self.fc1(state_action))
        q = t.relu(self.fc2(q))
        q = self.fc3(q)
        return q


if __name__ == "__main__":
    actor = Actor(observe_dim, action_dim, action_range)
    critic = Critic(observe_dim, action_dim)
    critic_t = Critic(observe_dim, action_dim)
    critic2 = Critic(observe_dim, action_dim)
    critic2_t = Critic(observe_dim, action_dim)

    sac = SAC(actor, critic, critic_t, critic2, critic2_t,
              t.optim.Adam,
              nn.MSELoss(reduction='sum'))

    episode, step, reward_fulfilled = 0, 0, 0
    smoothed_total_reward = 0
    save_actor(actor,'sac_start', 'Pendulum-v0', 'Osuna-Serris', 0)
    save_critic(critic,'sac_start', 'Pendulum-v0', 'Osuna-Serris','ante')

    while episode < max_episodes:
        episode += 1
        total_reward = 0
        terminal = False
        step = 0
        state = t.tensor(env.reset(), dtype=t.float32).view(1, observe_dim)

        while not terminal and step <= max_steps:
            step += 1
            with t.no_grad():
                old_state = state
                # agent model inference
                action = sac.act({"state": old_state})[0]
                state, reward, terminal, _ = env.step(action.numpy())
                state = t.tensor(state, dtype=t.float32).view(1, observe_dim)
                total_reward += reward[0]

                sac.store_transition({
                    "state": {"state": old_state},
                    "action": {"action": action},
                    "next_state": {"state": state},
                    "reward": reward[0],
                    "terminal": terminal or step == max_steps
                })
        all_rewards[episode] = total_reward
        # update, update more if episode is longer, else less
        if episode > 100:
            for _ in range(step):
                a_loss,c_loss = sac.update()
                actor_loss.append(a_loss)
                critic_loss.append(c_loss)
            if ((best_reward is None) or (total_reward > best_reward)):
                save_actor(actor,'sac_end', 'Pendulum-v0', 'Osuna-Serris')

        # show reward
        smoothed_total_reward = (smoothed_total_reward * 0.9 +
                                 total_reward * 0.1)
        logger.info("Episode {} total reward={:.2f}"
                    .format(episode, smoothed_total_reward))

        if episode == max_episodes:
            save_critic(critic,'sac_end', 'Pendulum-v0', 'Osuna-Serris','ante')
        # if smoothed_total_reward > solved_reward:
        #     reward_fulfilled += 1
        #     if reward_fulfilled >= solved_repeat:
        #         logger.info("Environment solved!")
        #         exit(0)
        # else:
        #     reward_fulfilled = 0
    actor_loss = np.array(actor_loss)
    critic_loss = np.array(critic_loss)
    with open('rewards_' + algo + '_' + str(max_episodes) + '_' + particularity + '_' + '.npy', 'wb') as f:
        np.save(f, all_rewards)
    with open('actor_loss_' + algo + '_' + str(max_episodes) + '_' + particularity + '_' + '.npy', 'wb') as f:
        np.save(f, actor_loss)
    with open('critic_loss_' + algo + '_' + str(max_episodes) + '_' + particularity + '_' + '.npy', 'wb') as f:
        np.save(f, critic_loss)